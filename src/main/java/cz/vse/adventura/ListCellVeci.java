package cz.vse.adventura;
import cz.vse.adventura.logika.Prostor;
import cz.vse.adventura.logika.Vec;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;

/**
 * Třída ListCellVeci slouží k definici zobrazení jednoho prvku v ListView, který obsahuje instance třídy Vec.
 * Každý prvek má textový popisek s názvem věci a grafický prvek s obrázkem věci na pozadí.
 */
public class ListCellVeci extends ListCell<Vec> {

    /**
     * Metoda updateItem se volá při aktualizaci obsahu buňky v ListView. Zajišťuje, aby každá buňka měla správný obsah
     * a vzhled na základě aktuální věci.
     *
     * @param vec   Aktuální instance třídy Vec, která bude zobrazena v buňce.
     * @param empty True, pokud je buňka prázdná; jinak false.
     */
    @Override
    protected void updateItem(Vec vec, boolean empty) {
        super.updateItem(vec, empty);
        if(empty){
            setText(null);
            setGraphic(null);
        }else{
            setText(vec.getNazev());
            String veci = getClass().getResource("veci/"+vec.getNazev()+".jpg").toExternalForm();
            ImageView iw = new ImageView(veci);
            iw.setFitHeight(100);
            iw.setPreserveRatio(true);
            setGraphic(iw);
        }
    }
}
