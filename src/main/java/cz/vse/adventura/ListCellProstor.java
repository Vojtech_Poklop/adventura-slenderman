package cz.vse.adventura;
import cz.vse.adventura.logika.Prostor;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;

/**
 * Třída ListCellProstor slouží k definici zobrazení jednoho prvku v ListView, který obsahuje instance třídy Prostor.
 * Každý prvek má textový popisek s názvem prostoru a grafický prvek s obrázkem prostoru na pozadí.
 */
public class ListCellProstor extends ListCell<Prostor> {

    /**
     *
     * Metoda updateItem se volá při aktualizaci obsahu buňky v ListView. Zajišťuje, aby každá buňka měla správný obsah
     * a vzhled na základě aktuálního prostoru.
     *
     * @param prostor Aktuální instance třídy Prostor, která bude zobrazena v buňce.
     * @param empty   True, pokud je buňka prázdná; jinak false.
     */
    @Override
    protected void updateItem(Prostor prostor, boolean empty) {
        super.updateItem(prostor, empty);
        if(empty){
            setText(null);
            setGraphic(null);
        }else{
            setText(prostor.getNazev());
            String cesta = getClass().getResource("mistnosti/"+prostor.getNazev()+".jpg").toExternalForm();
            ImageView iw = new ImageView(cesta);
            iw.setFitHeight(100);
            iw.setPreserveRatio(true);
            setGraphic(iw);
        }
    }
}
