package cz.vse.adventura.logika;

import cz.vse.adventura.Pozorovatel;
import cz.vse.adventura.ZmenaHry;
import cz.vse.adventura.uiText.TextoveRozhrani;
import javafx.application.Platform;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Trida Prostor - popisuje jednotlivé prostory (místnosti) hry
 *
 * Tato třída je součástí jednoduché textové hry.
 *
 * "Prostor" reprezentuje jedno místo (místnost, prostor, ..) ve scénáři hry.
 * Prostor může mít sousední prostory připojené přes východy. Pro každý východ
 * si prostor ukládá odkaz na sousedící prostor.
 *
 * @author Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Vojtěch Poklop
 * @version pro školní rok 2016/2017
 */
public class Prostor {

    private String nazev;
    private String popis;
    private Set<Prostor> vychody;   // obsahuje sousední místnosti


    Map<String, Vec> veci = new HashMap<>();

    private Map<ZmenaHry, Set<Pozorovatel>> seznamPozorovatelu = new HashMap<>();



    public Prostor(String nazev, String popis) {
    this.nazev =nazev;
    this.popis =popis;
    vychody =new HashSet<>();
    }

    /**
     * DÚ – návrhový vzor Factory využitý pro vytváření nových prostor.
     */
        public class Factory {
            public static Prostor vytvor(String nazev, String popis) {
                return new Prostor(nazev, popis);
            }
        }

    /**
     * Definuje východ z prostoru (sousední/vedlejsi prostor). Vzhledem k tomu,
     * že je použit Set pro uložení východů, může být sousední prostor uveden
     * pouze jednou (tj. nelze mít dvoje dveře do stejné sousední místnosti).
     * Druhé zadání stejného prostoru tiše přepíše předchozí zadání (neobjeví se
     * žádné chybové hlášení). Lze zadat též cestu ze do sebe sama.
     *
     * @param vedlejsi prostor, který sousedi s aktualnim prostorem.
     */
    public void addVychod(Prostor vedlejsi) {
        vychody.add(vedlejsi);
    }

    /**
     * Přidá věc do aktuálního prostoru.
     * @param vec
     */
    public void addVec(Vec vec) {
        veci.put(vec.getNazev(), vec);
    }

    /**
     * Odstraní věc z aktuálního prostoru.
     *
     * @param nazev
     * @return null
     */
    public Vec removeVec(String nazev) {
        veci.remove(nazev);
        return null;
    }

    /**
     * Zkontroluje, zdali se věc v aktulním prostoru nachází.
     *
     * @param nazev
     * @return název věci, pokud existuje
     */
    public boolean existujeVec(String nazev) {
        return veci.containsKey(nazev);
    }

    /**
     * Metoda equals pro porovnání dvou prostorů. Překrývá se metoda equals ze
     * třídy Object. Dva prostory jsou shodné, pokud mají stejný název. Tato
     * metoda je důležitá z hlediska správného fungování seznamu východů (Set).
     * <p>
     * Bližší popis metody equals je u třídy Object.
     *
     * @param o object, který se má porovnávat s aktuálním
     * @return hodnotu true, pokud má zadaný prostor stejný název, jinak false
     */
    @Override
    public boolean equals(Object o) {
        // porovnáváme zda se nejedná o dva odkazy na stejnou instanci
        if (this == o) {
            return true;
        }
        // porovnáváme jakého typu je parametr
        if (!(o instanceof Prostor)) {
            return false;    // pokud parametr není typu Prostor, vrátíme false
        }
        // přetypujeme parametr na typ Prostor
        Prostor druhy = (Prostor) o;

        //metoda equals třídy java.util.Objects porovná hodnoty obou názvů.
        //Vrátí true pro stejné názvy a i v případě, že jsou oba názvy null,
        //jinak vrátí false.

        return (Objects.equals(this.nazev, druhy.nazev));
    }

    /**
     * metoda hashCode vraci ciselny identifikator instance, ktery se pouziva
     * pro optimalizaci ukladani v dynamickych datovych strukturach. Pri
     * prekryti metody equals je potreba prekryt i metodu hashCode. Podrobny
     * popis pravidel pro vytvareni metody hashCode je u metody hashCode ve
     * tride Object
     */
    @Override
    public int hashCode() {
        int vysledek = 3;
        int hashNazvu = Objects.hashCode(this.nazev);
        vysledek = 37 * vysledek + hashNazvu;
        return vysledek;
    }

    /**
     * Vrací název prostoru (byl zadán při vytváření prostoru jako parametr
     * konstruktoru)
     *
     * @return název prostoru
     */
    public String getNazev() {
        return nazev;
    }

    /**
     * Vrací "dlouhý" popis prostoru, který může vypadat následovně: Jsi v
     * mistnosti/prostoru vstupni hala budovy VSE na Jiznim meste. vychody:
     * chodba bufet ucebna
     *
     * @return Dlouhý popis prostoru
     */
    public String dlouhyPopis() {
        return popis + "\n"
                + popisVychodu() + "\n"
                + popisVeci()
                ;//+ Slenderman()
    }

    /**
     * Vrací textový řetězec, který popisuje sousední východy, například:
     * "vychody: hala ".
     *
     * @return Popis východů - názvů sousedních prostorů
     */
    public String popisVychodu() {
        String vracenyText = "Východy: ";
        for (Prostor sousedni : vychody) {
            vracenyText += sousedni.getNazev() + ", ";
        }
        return vracenyText;
    }

    /**
     * Metoda, která pomocí cyklu for-each prohledá seznam veci a vypíše všechny věci, které se v
     * aktuálním prostoru nacházejí.
     *
     * @return věci, které se nacházejí v prostoru
     */

    public String popisVeci() {
        String vracenyText = "V prostoru se nachází: ";

        for (String nazevVeci : veci.keySet()) {
            vracenyText += nazevVeci + ", ";
        }

        if(veci.isEmpty()){
            vracenyText =  "V místnosti žádné věci nejsou.";
        }

        return vracenyText;
    }


    /**
     * Vrací prostor, který sousedí s aktuálním prostorem a jehož název je zadán
     * jako parametr. Pokud prostor s udaným jménem nesousedí s aktuálním
     * prostorem, vrací se hodnota null.
     *
     * @param nazevSouseda Jméno sousedního prostoru (východu)
     * @return Prostor, který se nachází za příslušným východem, nebo hodnota
     * null, pokud prostor zadaného jména není sousedem.
     */
    public Prostor vratSousedniProstor(String nazevSouseda) {
        List<Prostor> hledaneProstory =
                vychody.stream()
                        .filter(sousedni -> sousedni.getNazev().equals(nazevSouseda))
                        .collect(Collectors.toList());
        if (hledaneProstory.isEmpty()) {
            return null;
        } else {
            return hledaneProstory.get(0);
        }
    }

    /**
     * Vrací kolekci obsahující prostory, se kterými tento prostor sousedí.
     * Takto získaný seznam sousedních prostor nelze upravovat (přidávat,
     * odebírat východy) protože z hlediska správného návrhu je to plně
     * záležitostí třídy Prostor.
     *
     * @return Nemodifikovatelná kolekce prostorů (východů), se kterými tento
     * prostor sousedí.
     */
    public Collection<Prostor> getVychody() {
        return Collections.unmodifiableCollection(vychody);
    }

    public Map<String, Vec> getVeci() {
        return veci;
    }

    /**
     * Metoda sloužící k získání instance objektu třídy Vec na základě zadaného názvu.
     *
     * @param nazev -> název hledaného objektu.
     * @return instance Vec se zadaným názvem.
     * @throws IllegalStateException pokud se ve místnosti nenachází věc se zadaným názvem.
     */
    public Vec getVec(String nazev) throws IllegalStateException {
        Vec vec = veci.get(nazev);
        if (vec != null) {
            return vec;
        }
        throw new IllegalStateException("Věc " + nazev + " se v místnosti nenachází.");
    }

}



