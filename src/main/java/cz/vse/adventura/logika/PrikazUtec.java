package cz.vse.adventura.logika;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Třída PrikazUtec implementuje příkaz "uteč".
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */

public class PrikazUtec implements IPrikaz {

    public static final String NAZEV = "uteč";
    private Hra hra;

   public PrikazUtec(Hra hra) {
        this.hra = hra;
    }



    /**
     * Metoda vytvoří dva nové seznamy. Papírky potřebné na splnění úkolu a chybějící papírky.
     * Proběhne cyklus for-each. Pokud se v aktuální místnosti nenachází papírek, který je obsažen v seznamu potřebných papírků (seznam papirky),
     * přidá se do seznamu chybejiciPapirky.
     *
     * Pokud se v aktuální místnosti nachází věc "vrata" (úniková místnost) a seznam chybejiciPapírky je prázdný (žádné papírky nechybí),
     * hráč vyhraje hru -> hra se ukonční
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return string "nemůžeš utéct", hráč není ve správné místnost, string "nemůžeš utéct", hráč nemá všechny papírky + vypíš chybějících papírků,
     *          string "utekl jsi".
     */

    public String provedPrikaz(String... parametry) {
        StringBuilder vystup = new StringBuilder();

        try {
            List<String> chybejiciPapirky = new ArrayList<>();
            List<String> papirky = Arrays.asList("papírekA", "papírekB", "papírekC", "papírekD");

            for (String papirek : papirky) {
                if (!hra.getHerniPlan().getAktualniProstor().existujeVec(papirek)) {
                    chybejiciPapirky.add(papirek);
                }
            }

            if (!hra.getHerniPlan().getAktualniProstor().existujeVec("vrata")) {
                vystup.append("Nemůžeš utéct, nenacházíš se u východu.\n");
            } else if (chybejiciPapirky.isEmpty() && hra.getHerniPlan().getAktualniProstor().existujeVec("vrata")) {
                hra.setKonecHry(true);
                vystup.append("Podařilo se ti utéct.\nVyhrál jsi, gratulujeme, hra se nyní ukončí.\n");
            } else {
                vystup.append("Nemůžeš utéct, u východu chybí následující papírky:\n");
                for (String papirek : chybejiciPapirky) {
                    vystup.append("- ").append(papirek).append("\n");
                }
            }
        } catch (IllegalStateException exception) {
            vystup.append(exception.getMessage());
        }

        return vystup.toString();
    }



    @Override
    public String getNazev() {
        return NAZEV;
    }

}

