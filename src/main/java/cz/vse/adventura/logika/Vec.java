package cz.vse.adventura.logika;

/**
 * Třída inicialiující objekt věc (jeho název a přenositelnost).
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */

public class Vec {

    private String nazev;
    private boolean prenositelna;

    public Vec(String nazev, boolean prenositelna) {
        this.nazev = nazev;
        this.prenositelna = prenositelna;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public boolean isPrenositelna() {
        return prenositelna;
    }

}
