package cz.vse.adventura.logika;

import cz.vse.adventura.ZmenaHry;

import java.util.HashSet;

/**
 * Třída PrikazPoloz implementuje příkaz "polož".
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */
public class PrikazPoloz implements IPrikaz {
    public static final String NAZEV = "polož";
    private HerniPlan plan;

    public PrikazPoloz(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda, pokud se v batohu nachází, položí věc do aktuálního prostoru.
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return název věci + úspěch nebo neúspěch.
     */

    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Nenapsal jsi, jakou věc mám z batohu vyndat.";
        }
        String nazevVeci = parametry[0];

        boolean result = plan.polozVec(nazevVeci);
        plan.upozorniPozorovatele(ZmenaHry.ZMENA_VECI_V_MISTNOSTI);

        if (result) {
            return "Věc " + nazevVeci + " byla vložena do místnosti.";
        } else {
            return "Věc " + nazevVeci + " se v batohu nenachází.";
        }
    }


    @Override
    public String getNazev() {
        return NAZEV;
    }

}
