package cz.vse.adventura.logika;

import java.util.Iterator;

/**
 * Třída PrikazVypisBatoh implementuje příkaz "vypiš_batoh".
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */

public class PrikazVypisBatoh implements IPrikaz {

    public static final String NAZEV = "vypiš_batoh";
    private HerniPlan plan;

    public PrikazVypisBatoh(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda projde seznam Batoh a vypíše věci v něm obsažené.
     * DÚ – místo klasického for each použit návrhový vzor Iterator.
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return výpis věcí z batohu, nebo string "Batoh je prázdný.".
     */

    public String provedPrikaz(String... parametry) {

            String popis = "";
            if (plan.getBatoh().veciVBatohu.isEmpty()) {
                popis += "Batoh je prázdný.\n";
            } else {
                Iterator<Vec> iterator = plan.getBatoh().veciVBatohu.iterator();
                while (iterator.hasNext()) {
                    Vec vec = iterator.next();
                    popis += "- " + vec.getNazev() + "\n";
                }
            }

        return "\nV batohu se nachází: \n" + popis;
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }

}
