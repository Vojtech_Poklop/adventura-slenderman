package cz.vse.adventura.logika;

/**
 *  Třída PrikazNapoveda implementuje pro hru příkaz napoveda.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček
 *@version    pro školní rok 2016/2017
 *  
 */
public class PrikazNapoveda implements IPrikaz {
    
    public static final String NAZEV = "nápověda";
    private SeznamPrikazu platnePrikazy;
    
    
     /**
    *  Konstruktor třídy
    *  
    *  @param platnePrikazy seznam příkazů,
    *                       které je možné ve hře použít,
    *                       aby je nápověda mohla zobrazit uživateli. 
    */    
    public PrikazNapoveda(SeznamPrikazu platnePrikazy) {
        this.platnePrikazy = platnePrikazy;
    }
    
    /**
     *  Vrací základní nápovědu po zadání příkazu "napoveda". Nyní se vypisuje
     *  vcelku primitivní zpráva a seznam dostupných příkazů.
     *  
     *  @return napoveda ke hře
     */
    @Override
    public String provedPrikaz(String... parametry) {
        return "Tvým úkolem je utéct z objektu a nenechat se polapit Slendermanem.\n"
        + "Sesbírej papírky A, B, C, D, polož je do místnosti, kde se nachází východ a proveď příkaz ''uteč''.\n"
        + "V batohu uneseš maximálně tři předměty.\n"
        + "Abys mohl projít přes zamčené dveře, musíš najít klíč stejné barvy, jaké jsou dveře a provést příkaz ''otevři''\n"
        + "\n"
        + "Můžeš zadat tyto příkazy:\n"
        + platnePrikazy.vratNazvyPrikazu();
    }
    
     /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
      public String getNazev() {
        return NAZEV;
     }

}
