package cz.vse.adventura.logika;

/**
 * Třída PrikazVypisVychody implementuje příkaz "vypiš_východy".
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */

class PrikazVypisVychody implements IPrikaz {
    private static final String NAZEV = "vypiš_východy";
    private HerniPlan plan;

    public PrikazVypisVychody(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda pro aktuální prostor zavolá metodu popisVychodu(), která vypíše všechny východy, které se
     * nacházejí v aktuální mistnosti.
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return východy, které se nacházejí v místnosti.
     */

    public String provedPrikaz(String... parametry) {
        String vypisTo = "";
        Prostor aktualniProstor = plan.getAktualniProstor();

        vypisTo = aktualniProstor.popisVychodu();

        return "\n" + vypisTo;
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }

}
