package cz.vse.adventura.logika;

import cz.vse.adventura.ZmenaHry;

/**
 * Třída PrikazOtevri implementuje příkaz "otevři".
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */
public class PrikazZivoty implements IPrikaz {

    public static final String NAZEV = "vypiš_životy";
    private HerniPlan plan;
    public PrikazZivoty(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda vrací počet zbývajících životů.
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return příkaz se provede -> prázdný string, nebo se vyhodí chyba
     */
    public String provedPrikaz(String... parametry) {

        try {

            return "Nyní máš " + plan.zivoty.size() + " hp. ";

        } catch (IllegalStateException exception) {
            return exception.getMessage();
        }
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }

}
