package cz.vse.adventura.logika;

import cz.vse.adventura.ZmenaHry;

/**
 * Třída PrikazSeber implementuje příkaz "seber".
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */

public class PrikazSeber implements IPrikaz {
    public static final String NAZEV = "seber";
    private HerniPlan plan;

    public PrikazSeber(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda zavolá metodu seberVec ze třidy HerniPlan, která odstraní věc z aktuálního prostoru a vloží ji do batohu
     * (pokud se v místnosti nachází a jde sebrat).
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return obsah seznamu Batohu, nebo chyba.
     */

    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            return "Nenapsal jsi, jakou  věc mám z prostoru sebrat.";
        }
        String nazevVeci = parametry[0];

        try {

            plan.seberVec(nazevVeci);
            plan.upozorniPozorovatele(ZmenaHry.ZMENA_VECI_V_MISTNOSTI);
            if(plan.isUspesne()==true){
                plan.setUspesne(false);
                return "Vloženo do inventáře";
            }else if(plan.isUspesne()==false){
                return "Nejde sebrat, nebo máte plný batoh.";
            }

        } catch (IllegalStateException exception) {
            return exception.getMessage();
        }
        plan.upozorniPozorovatele(ZmenaHry.ZMENA_VECI_V_BATOHU);

        return "";
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */

    @Override
    public String getNazev() {
        return NAZEV;
    }

}
