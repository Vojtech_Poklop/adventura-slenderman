package cz.vse.adventura.logika;

import cz.vse.adventura.Pozorovatel;
import cz.vse.adventura.PredmetPozorovani;
import cz.vse.adventura.ZmenaHry;
import javafx.fxml.FXML;

import java.util.*;

/**
 * Třída, která inicializuje seznam batoh, do kterého lze vkládat předměty.
 * Batoh slouží k uchovávání a manipulaci s předměty ve hře.
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */

public class Batoh {

    private HashSet<Batoh> batoh;
    public int maxKapacita = 3;

    private static Batoh instanceSingleton = null;

    public Batoh() {
        this.batoh = new HashSet<>();
    }
    HashSet<Vec> veciVBatohu = new HashSet<>();

    /**
        DÚ – návrhový vzor Singleton. Ve hře může být pouze jeden batoh. Pokud
        by byl nějakým způsobem vytvořen batoh druhý, bude ale sdílet jeden
        HashSet s původním batohem.
         */
    public static Batoh getInstance(){
        if(instanceSingleton == null){
            instanceSingleton = new Batoh();
        }
        return instanceSingleton;
    }

    /**
     * Metoda, která přidává věc do seznamu Batoh, pokud není Batoh plný a věc je přenositelná.
     *
     * @param vec, která má být přidána do batohu.
     * @return true -> věc byla přidána do batohu, false -> věc se nepodařilo přidat do batohu.
     */

    public boolean pridejVec(Vec vec){

        if(isObsazeno() == false && vec.isPrenositelna() == true) {
            veciVBatohu.add(vec);


            return true;
        }else if (isObsazeno() == true || vec.isPrenositelna() == false){
            return false;
        }
        return true;
    }

    /**
     * Metoda pomocí cyklu for-each projde seznam Batoh a vrátí instanci Věc.
     *
     * @param nazev název věci
     * @return vrátí název věci
     * @throws IllegalStateException -> věc v batohu není
     */
    public Vec getVec(String nazev) throws IllegalStateException {
        for (Vec vec : veciVBatohu) {
            if (vec.getNazev().equals(nazev)) {
                return vec;
            }
        }
        throw new IllegalStateException("Věc " + nazev + " se v batohu nenachází.");
    }


    /**
     * Seznam Batoh má svoji kapacitu. Pokud je dosažen limit pro přidávání nových věcí, metoda nedovolí další věc přidat.
     *
     * @return true -> další věc může být přidána, false -> obsazeno, již nelze nic přidat.
     */
    public boolean isObsazeno() {

        int kapacita = veciVBatohu.size();

        if (kapacita >= maxKapacita) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Metoda projde seznam Batoh a vypíše věci v něm obsažené.
     * DÚ – místo klasického for each použit návrhový vzor Iterator.
     *
     * @return věci v Batohu, nebo string, že je batoh prázdný.
     */
    public String getPopisObsahuBatohu() {

        String popis = "";
        if (veciVBatohu.isEmpty()) {
            popis += "Batoh je prázdný.\n";
        } else {
            Iterator<Vec> iterator = veciVBatohu.iterator();
            while (iterator.hasNext()) {
                Vec vec = iterator.next();
                popis += "- " + vec.getNazev() + "\n";
            }
        }
        return "\nV batohu se nachází: \n" + popis;
    }

    public Collection<Vec> vypisBatohCollection(){
        Set<Vec> seznam = new HashSet<>();
        seznam.addAll(veciVBatohu);

        return seznam;
    }
}
