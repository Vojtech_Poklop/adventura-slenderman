package cz.vse.adventura.logika;

import cz.vse.adventura.Pozorovatel;
import cz.vse.adventura.PredmetPozorovani;
import cz.vse.adventura.ZmenaHry;
import javafx.fxml.FXML;

import java.util.*;

/**
 *  Class HerniPlan - třída představující mapu a stav adventury.
 * 
 *  Tato třída inicializuje prvky ze kterých se hra skládá:
 *  vytváří všechny prostory,
 *  propojuje je vzájemně pomocí východů 
 *  a pamatuje si aktuální prostor, ve kterém se hráč právě nachází.
 *
 *@author     Michael Kolling, Lubos Pavlicek, Jarmila Pavlickova, Vojtěch Poklop
 *@version    pro školní rok 2016/2017
 */
public class HerniPlan implements PredmetPozorovani {

    private Batoh batoh;
    private Prostor aktualniProstor;
    private boolean uspesne = false;
    List<Integer> zivoty = new ArrayList<>(Arrays.asList(1, 2));
    private Map<String, Prostor> vychody2;
    private Map<ZmenaHry, Set<Pozorovatel>> seznamPozorovatelu = new HashMap<>();

    /**
     * Konstruktor který vytváří jednotlivé prostory a propojuje je pomocí východů.
     * Jako výchozí aktuální prostor nastaví halu.
     */
    public HerniPlan() {
        this.batoh = Batoh.getInstance();
        zalozProstoryHry();
        for(ZmenaHry zmenaHry: ZmenaHry.values()){
            seznamPozorovatelu.put(zmenaHry, new HashSet<>());
        }
    }

    /**
     * Vytváří jednotlivé prostory a propojuje je pomocí východů.
     * Jako výchozí aktuální prostor nastaví domeček.
     */
    private void zalozProstoryHry() {

        vychody2 = new HashMap<>();

        Prostor lesV = Prostor.Factory.vytvor("východníLes", "Nacházíš se ve východním lese.");
        Prostor lesZ = Prostor.Factory.vytvor("západníLes", "Nacházíš se v západním lese.");

        Prostor zahJ = Prostor.Factory.vytvor("jižníZahrada", "Nacházíš se v jižní části kůlny.");
        Prostor zahV = Prostor.Factory.vytvor("východníZahrada", "Nacházíš se ve východní části kůlny.");
        Prostor zahZ = Prostor.Factory.vytvor("západníZahrada", "Nacházíš se v západní části kůlny.");

        Prostor rPalV = Prostor.Factory.vytvor("východníRudýPalác", "Nacházíš se ve východních komnatách rudého paláce.");
        Prostor rPalZ = Prostor.Factory.vytvor("západníRudýPalác", "Nacházíš se v západních komnatách rudého paláce.");
        Prostor rPalS = Prostor.Factory.vytvor("severníRudýPalác", "Nacházíš se v severních komnatách rudého paláce.");

        Prostor zPalV = Prostor.Factory.vytvor("východníŽlutýPalác", "Nacházíš se ve východních komnatách žlutého paláce.");
        Prostor zPalZ = Prostor.Factory.vytvor("západníŽlutýPalác", "Nacházíš se v západních komnatách žlutého paláce.");
        Prostor zPalS = Prostor.Factory.vytvor("severníŽlutýPalác", "Nacházíš se v severních komnatách žlutého paláce.");

        Prostor vychod = Prostor.Factory.vytvor("východZAreálu", "Polož sem 5 papírku a můžeš utéct");


        Vec papirek1 = new Vec("papírekA", true);
        rPalZ.addVec(papirek1);
        Vec papirek2 = new Vec("papírekB", true);
        zahJ.addVec(papirek2);
        Vec papirek3 = new Vec("papírekC", true);
        lesV.addVec(papirek3);
        Vec papirek4 = new Vec("papírekD", true);
        zPalV.addVec(papirek4);

        Vec Rdvere = new Vec("rudéDveře", false);
        lesZ.addVec(Rdvere);
        Vec Zdvere = new Vec("žlutéDveře", false);
        lesV.addVec(Zdvere);
        Vec Zklic = new Vec("žlutýKlíč", true);
        rPalZ.addVec(Zklic);
        Vec Rklic = new Vec("rudýKlíč", true);
        zahZ.addVec(Rklic);

        Vec slenderMan1 = new Vec("SLENDERMAN!", false);
        rPalS.addVec(slenderMan1);
        Vec slenderMan2 = new Vec("SLENDERMAN!", false);
        zahV.addVec(slenderMan2);
        Vec slenderMan3 = new Vec("SLENDERMAN!", false);
        zPalS.addVec(slenderMan3);

        Vec vrata = new Vec("vrata", false);
        vychod.addVec(vrata);

        vychody2.put("žlutýPalác",zPalZ);
        vychody2.put("rudýPalác", rPalV);


        lesV.addVychod(lesZ);
        lesV.addVychod(zahJ);
        lesZ.addVychod(lesV);
        lesZ.addVychod(zahJ);

        zahJ.addVychod(zahZ);
        zahJ.addVychod(zahV);
        zahJ.addVychod(lesZ);
        zahJ.addVychod(lesV);
        zahZ.addVychod(zahJ);
        zahZ.addVychod(vychod);
        zahZ.addVychod(zahV);
        zahV.addVychod(zahZ);
        zahV.addVychod(zahJ);

        zPalZ.addVychod(lesV);
        zPalZ.addVychod(zPalS);
        zPalZ.addVychod(zPalV);
        zPalS.addVychod(zPalZ);
        zPalS.addVychod(zPalV);
        zPalV.addVychod(zPalS);
        zPalV.addVychod(zPalZ);

        rPalV.addVychod(lesZ);
        rPalV.addVychod(rPalS);
        rPalV.addVychod(rPalZ);
        rPalZ.addVychod(rPalS);
        rPalZ.addVychod(rPalV);
        rPalS.addVychod(rPalZ);
        rPalS.addVychod(rPalV);

        vychod.addVychod(zahZ);

        aktualniProstor = lesZ;

    }
    public Map<String, Prostor> getVychody2() {
        return vychody2;
    }
    public Prostor getAktualniProstor() {
        return aktualniProstor;
    }
    /**
     * Metoda nastaví aktuální prostor, používá se nejčastěji při přechodu mezi prostory
     *
     * @param prostor nový aktuální prostor
     */
    public void setAktualniProstor(Prostor prostor) {
        aktualniProstor = prostor;
        upozorniPozorovatele(ZmenaHry.ZMENA_MISTNOSTI);
    }


    /**
     * Metoda se pokusí, pokud je to možné, vložit věc do seznamu Batoh.
     *
     * @param nazev věci
     * @return věc, nebo null.
     */

    public Vec seberVec(String nazev) throws IllegalStateException {
        Vec vec = this.getAktualniProstor().getVec(nazev);
        if (batoh.pridejVec(vec) == true) {
            this.getAktualniProstor().removeVec(nazev);
            uspesne=true;
            return vec;
        } else {
            return null;
        }
    }

    /**
     * Metoda, pokud se v Batohu nachází, položí věc zpátky do místnosti.
     *
     * @param nazev věci
     * @return true -> věc položena do místnosti, false -> nepodařilo se vložit do místnosti.
     */
    public boolean polozVec(String nazev) throws IllegalStateException {
        for (Vec vec : batoh.veciVBatohu) {
            if (vec.getNazev().equals(nazev)) {
                batoh.veciVBatohu.remove(vec);
                aktualniProstor.addVec(vec);
                return true;
            }
        }
        return false;
    }

    public Batoh getBatoh() {
        return batoh;
    }


    /**
     * Metoda registruje pozorovatele pro sledování změn v hře.
     * Pozorovatel je přidán ke konkrétnímu typu změny hry.
     *
     * @param zmenaHry      Typ změny hry, ke kterému se přidává pozorovatel.
     * @param pozorovatel   Pozorovatel, který bude sledovat změny v hře.
     */
    @Override
    public void registruj(ZmenaHry zmenaHry, Pozorovatel pozorovatel) {
        seznamPozorovatelu.get(zmenaHry).add(pozorovatel);
    }

    /**
     * Metoda upozorní všechny registrované pozorovatele o změně hry.
     *
     * @param zmenaHry Typ změny hry, o které se informuje pozorovatele.
     */
    @FXML
    public void upozorniPozorovatele(ZmenaHry zmenaHry) {
        for(Pozorovatel pozorovatel: seznamPozorovatelu.get(zmenaHry)){
            pozorovatel.aktualizuj();
        }
    }

    /**
     * Metoda kontroluje, zdali se v prostoru nachází Slenderman. Pokud ano, odstraní jeden život z List<Integer> zivoty.
     *
     * @return informace o stavu životů a případné smrti hráče.
     */
    protected String Slenderman() {
        String druhyText = "Zemřel jsi, hra se nyní ukončí.";

        if(aktualniProstor.veci.containsKey("SLENDERMAN!")) {
            if (!zivoty.isEmpty()) {
                zivoty.remove(0);
            }
            return druhyText;
        }
        return "";
    }
    protected String pocetZivotu(){
        return "\n"+"Ztráčíš 1 hp."+"\n"+"Nyní máš " + zivoty.size() + " hp. ";
    }

    public boolean isUspesne() {
        return uspesne;
    }

    public void setUspesne(boolean uspesne) {
        this.uspesne = uspesne;
    }

}