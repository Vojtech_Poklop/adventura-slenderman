package cz.vse.adventura.logika;

/**
 * Třída PrikazVypisMistnost implementuje příkaz "vypiš_místnost".
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */

class PrikazVypisMistnost implements IPrikaz {
    private static final String NAZEV = "vypiš_místnost";
    private HerniPlan plan;

    public PrikazVypisMistnost(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda pro aktuální prostor zavolá metodu popisVeci(), která vypíše všechny věci, které se
     * nacházejí v aktuální mistnosti.
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return věci, které se nacházejí v místnosti.
     */

    public String provedPrikaz(String... parametry) {
        String vypisTo = "";
        Prostor aktualniProstor = plan.getAktualniProstor();

        vypisTo = aktualniProstor.popisVeci();

        return "\nV místnosti se nachází: \n" + vypisTo;
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }

}
