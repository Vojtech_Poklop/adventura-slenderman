package cz.vse.adventura.logika;

import cz.vse.adventura.ZmenaHry;

/**
 *  Třída PrikazJdi implementuje pro hru příkaz jdi.
 *  Tato třída je součástí jednoduché textové hry.
 *  
 *@author     Jarmila Pavlickova, Luboš Pavlíček
 *@version    pro školní rok 2016/2017
 */
public class PrikazJdi implements IPrikaz {
    public static final String NAZEV = "jdi";
    private HerniPlan plan;
    private Hra hra;
    
    /**
    *  Konstruktor třídy
    *  
    *  @param hra herní plán, ve kterém se bude ve hře "chodit"
    */    
    public PrikazJdi(Hra hra) {
        this.hra = hra;
    }

    /**
     *  Provádí příkaz "jdi". Zkouší se vyjít do zadaného prostoru. Pokud prostor
     *  existuje, vstoupí se do nového prostoru. Pokud zadaný sousední prostor
     *  (východ) není, vypíše se chybové hlášení.
     *
     *  Pokud je List<Integer> zivoty prázdný, ukonční hru.
     *  Pokud se v místnosti nachází Slenderman, místo normálního popisu místnosti vrátí i zbývající počet životů
     *
     *@param parametry - jako  parametr obsahuje jméno prostoru (východu),
     *                         do kterého se má jít.
     *@return zpráva, kterou vypíše hra hráči
     */ 
    @Override
    public String provedPrikaz(String... parametry) {
        if (parametry.length == 0) {
            // pokud chybí druhé slovo (sousední prostor), tak ....
            return "Kam mám jít? Musíš zadat jméno východu";
        }

        String smer = parametry[0];

        // zkoušíme přejít do sousedního prostoru
        Prostor sousedniProstor = hra.getHerniPlan().getAktualniProstor().vratSousedniProstor(smer);


        if (sousedniProstor == null) {
            return "Tam se odsud jít nedá!";
        }
        else {
            hra.getHerniPlan().setAktualniProstor(sousedniProstor);
            hra.getHerniPlan().upozorniPozorovatele(ZmenaHry.ZMENA_VECI_V_MISTNOSTI);
            hra.getHerniPlan().Slenderman();

            if(hra.getHerniPlan().zivoty.isEmpty()){
                hra.setKonecHry(true);
                return sousedniProstor.dlouhyPopis()+hra.getHerniPlan().pocetZivotu()+hra.getHerniPlan().Slenderman();
            }
            if(hra.getHerniPlan().getAktualniProstor().existujeVec("SLENDERMAN!")){
                return  sousedniProstor.dlouhyPopis()+hra.getHerniPlan().pocetZivotu();
            }

            return sousedniProstor.dlouhyPopis();
        }
    }

    /**
     *  Metoda vrací název příkazu (slovo které používá hráč pro jeho vyvolání)
     *  
     *  @ return nazev prikazu
     */
    @Override
    public String getNazev() {
        return NAZEV;
    }


}
