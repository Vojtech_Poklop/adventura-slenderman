package cz.vse.adventura.logika;

import cz.vse.adventura.ZmenaHry;

/**
 * Třída PrikazOtevri implementuje příkaz "otevři".
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */
public class PrikazOtevri implements IPrikaz {

    public static final String NAZEV = "otevři";
    private HerniPlan plan;
    public PrikazOtevri(HerniPlan plan) {
        this.plan = plan;
    }

    /**
     * Metoda "otevírá" dveře.
     * Pokud se v prostoru nachází dveře, v Batohu klíč a jejich barva je stejná, obě věci se odstraní a seznamu s východy přibude nový východ.
     * Každé otevření dveří upozorní pozorovatele.
     *
     * @param parametry počet parametrů závisí na konkrétním příkazu.
     * @return příkaz se provede -> prázdný string, nebo se vyhodí chyba
     */
    public String provedPrikaz(String... parametry) {

        try {

            if (plan.getAktualniProstor().existujeVec("žlutéDveře")) {
                Vec zlutyKlic = plan.getBatoh().getVec("žlutýKlíč");
                if (zlutyKlic != null) {
                    plan.getBatoh().veciVBatohu.remove(zlutyKlic);
                    plan.getAktualniProstor().removeVec("žlutéDveře");
                    plan.getAktualniProstor().addVychod(plan.getVychody2().get("žlutýPalác"));
                    plan.upozorniPozorovatele(ZmenaHry.ZMENA_MISTNOSTI);
                    plan.upozorniPozorovatele(ZmenaHry.ZMENA_VECI_V_BATOHU);
                    plan.upozorniPozorovatele(ZmenaHry.ZMENA_VECI_V_MISTNOSTI);
                    return "Žluté dveře se nyní otevřou. \n" + plan.getAktualniProstor().popisVychodu();

                }
            } else if (plan.getAktualniProstor().existujeVec("rudéDveře")) {
                Vec rudyKlic = plan.getBatoh().getVec("rudýKlíč");
                if (rudyKlic != null) {
                    plan.getBatoh().veciVBatohu.remove(rudyKlic);
                    plan.getAktualniProstor().removeVec("rudéDveře");
                    plan.getAktualniProstor().addVychod(plan.getVychody2().get("rudýPalác"));
                    plan.upozorniPozorovatele(ZmenaHry.ZMENA_MISTNOSTI);
                    plan.upozorniPozorovatele(ZmenaHry.ZMENA_VECI_V_BATOHU);
                    plan.upozorniPozorovatele(ZmenaHry.ZMENA_VECI_V_MISTNOSTI);
                    return "Rudé dveře se nyní otevřou. \n" + plan.getAktualniProstor().popisVychodu();
                }
            }else{
                return "Nejde nic otevřít";}


        } catch (IllegalStateException exception) {
            return exception.getMessage();
        }
        return "";
    }

    @Override
    public String getNazev() {
        return NAZEV;
    }

}
