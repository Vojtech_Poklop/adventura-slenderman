package cz.vse.adventura;

/**
 * observer
 */

public interface Pozorovatel {
    void aktualizuj();
}
