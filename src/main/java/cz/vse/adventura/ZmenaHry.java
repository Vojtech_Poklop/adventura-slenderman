package cz.vse.adventura;

public enum ZmenaHry {
    ZMENA_MISTNOSTI,
    KONEC_HRY,
    ZMENA_VECI_V_BATOHU,
    ZMENA_VECI_V_MISTNOSTI
}
