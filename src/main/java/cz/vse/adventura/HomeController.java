package cz.vse.adventura;

import cz.vse.adventura.logika.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Point2D;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;
import javafx.stage.Stage;
import javafx.util.Callback;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class HomeController {
    @FXML
    private ImageView grafikaOtevri;
    @FXML
    private ImageView grafikaZivoty;
    @FXML
    private ImageView grafikaNapoveda;
    @FXML
    private ImageView grafikaUtec;
    @FXML
    private ImageView grafikaBatoh;
    @FXML
    private ImageView hrac;
    @FXML
    private Button tlacitkoOdesli;
    @FXML
    private ListView<Prostor> panelVychodu;
    @FXML
    private ListView<Vec> panelVeci;
    @FXML
    public ListView<Vec> panelVeciVMistnosti;
    @FXML
    private TextArea vystup;

    @FXML
    private TextField vstup;

    private IHra hra = new Hra();
    private Prostor prostor;

    private ObservableList<Prostor> seznamVychodu = FXCollections.observableArrayList();
    private ObservableList<Vec> seznamVeciVBatohu = FXCollections.observableArrayList();
    private ObservableList<Vec> seznamVeciVMistnosti = FXCollections.observableArrayList();

    /**
     * Inicializační metoda pro nastavení počátečního stavu uživatelského rozhraní.
     * Volá se automaticky po načtení FXML souboru a inicializuje komponenty a event listenery.
     */
    @FXML
    private void initialize(){
        vystup.appendText(hra.vratUvitani()+"\n\n");
        Platform.runLater(()->vstup.requestFocus());
        panelVychodu.setItems(seznamVychodu);
        panelVeci.setItems(seznamVeciVBatohu);
        panelVeciVMistnosti.setItems(seznamVeciVMistnosti);

        hra.getHerniPlan().registruj(ZmenaHry.ZMENA_MISTNOSTI, () -> {
            aktualizujSeznamVychodu();
            aktualizujPolohuHrace();
        });

        hra.getHerniPlan().registruj(ZmenaHry.ZMENA_VECI_V_BATOHU ,() ->
                aktualizujVeciVBatohu());

        hra.getHerniPlan().registruj(ZmenaHry.ZMENA_VECI_V_MISTNOSTI, () ->
                aktualizujVeciVBatohu());

        hra.getHerniPlan().registruj(ZmenaHry.ZMENA_VECI_V_MISTNOSTI, ()->
                aktualizujVeciVMistnosti());
        aktualizujVeciVBatohu();

        hra.registruj(ZmenaHry.KONEC_HRY, () -> aktualizujKonecHry());
        aktualizujSeznamVychodu();
        aktualizujVeciVMistnosti();
        aktualizujVeciVBatohu();
        vlozSouradnice();

        panelVychodu.setCellFactory(param -> new ListCellProstor());
        panelVeci.setCellFactory(param -> new ListCellVeci());
        panelVeciVMistnosti.setCellFactory(param -> new ListCellVeciVMistnosti());
    }

    private Map<String, Point2D> souradniceProstoru = new HashMap<>();


    /**
     * Vkládá souřadnice pro pohyb po mapě.
     */
    private void vlozSouradnice() {
        souradniceProstoru.put("západníLes", new Point2D(269, 253));
        souradniceProstoru.put("východníLes", new Point2D(367, 253));
        souradniceProstoru.put("jižníZahrada", new Point2D(319, 177));
        souradniceProstoru.put("východníZahrada", new Point2D(368, 104));
        souradniceProstoru.put("západníZahrada", new Point2D(270, 104));
        souradniceProstoru.put("východníRudýPalác", new Point2D(169, 253));
        souradniceProstoru.put("západníRudýPalác", new Point2D(69, 253));
        souradniceProstoru.put("severníRudýPalác",new Point2D(119, 177));
        souradniceProstoru.put("východníŽlutýPalác", new Point2D(566, 253));
        souradniceProstoru.put("západníŽlutýPalác", new Point2D(466, 253));
        souradniceProstoru.put("severníŽlutýPalác", new Point2D(516, 177));
        souradniceProstoru.put("východZAreálu", new Point2D(220, 29));
    }

    @FXML
    private void aktualizujSeznamVychodu(){
        seznamVychodu.clear();
        seznamVychodu.addAll(hra.getHerniPlan().getAktualniProstor().getVychody());
    }

    private void aktualizujPolohuHrace(){
        String prostor = hra.getHerniPlan().getAktualniProstor().getNazev();
        hrac.setLayoutX(souradniceProstoru.get(prostor).getX());
        hrac.setLayoutY(souradniceProstoru.get(prostor).getY());
    }


    private void aktualizujVeciVBatohu(){
        seznamVeciVBatohu.clear();
        seznamVeciVBatohu.addAll(hra.getHerniPlan().getBatoh().vypisBatohCollection());
    }

    private void aktualizujVeciVMistnosti(){
        seznamVeciVMistnosti.clear();
        seznamVeciVMistnosti.addAll(hra.getHerniPlan().getAktualniProstor().getVeci().values());
    }


    /**
     * Metoda pro obsluhu akce odeslání příkazu. Získá text z vstupního pole, vyčistí pole a zpracuje zadaný příkaz.
     *
     * @param actionEvent Akce spojená s odesláním formuláře nebo stisknutím klávesy Enter.
     */
    @FXML
    private void odesliVstup(ActionEvent actionEvent) {
        String prikaz = vstup.getText();
        vstup.clear();
        zpracujPrikaz(prikaz);
    }

    /**
     * Metoda pro zpracování zadaného příkazu. Přidá zadaný příkaz do výstupního textového pole,
     * následně zavolá metodu pro zpracování příkazu ve hře a výsledek opět zobrazí ve výstupním poli.
     *
     * @param prikaz Zadaný příkaz od hráče.
     */
    private void zpracujPrikaz(String prikaz) {
        vystup.appendText(" > "+ prikaz +"\n");
        String vysledek = hra.zpracujPrikaz(prikaz);
        vystup.appendText(vysledek+"\n\n");
    }

    /**
     * Metoda pro ukončení hry po stisknutí tlačítka nebo provedení akce. Zobrazí dialogové okno typu CONFIRMATION
     * s otázkou, zda si hráč přeje skutečně ukončit hru. Po potvrzení provede ukončení hry.
     *
     * @param actionEvent Akce spojená s provedením ukončení hry, například stisknutím tlačítka.
     */
    public void ukoncitHru (ActionEvent actionEvent){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Skutečně si přejete ukončit hru?");
        alert.showAndWait();
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            Platform.exit();
        }
    }

    /**
     * Metoda pro restartování hry po stisknutí tlačítka nebo provedení akce. Vytvoří novou instanci hry, inicializuje
     * herní grafiku a znovu povolí ovládací prvky, které mohou být dočasně zakázány.
     *
     * @param actionEvent Akce spojená s provedením restartu hry, například stisknutím tlačítka.
     */
    public void restartHry(ActionEvent actionEvent) {

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Skutečně si přejete ukončit hru?");
        alert.showAndWait();
        Optional<ButtonType> result = alert.showAndWait();
        if (result.isPresent() && result.get() == ButtonType.OK) {
            hra = new Hra();

            aktualizujVeciVMistnosti();
            aktualizujPolohuHrace();
            aktualizujSeznamVychodu();
            aktualizujVeciVBatohu();

            initialize();
            vstup.setDisable(false);
            tlacitkoOdesli.setDisable(false);
            panelVychodu.setDisable(false);
            panelVeci.setDisable(false);
            panelVeciVMistnosti.setDisable(false);
            grafikaZivoty.setDisable(false);
            grafikaBatoh.setDisable(false);
            grafikaNapoveda.setDisable(false);
            grafikaUtec.setDisable(false);
            grafikaOtevri.setDisable(false);
        }
    }

    /**
     * Metoda po konci hry vrátí epilog zakáže všechny ovládací prvky.
     */
    private void aktualizujKonecHry() {
        if(hra.konecHry()) {
            hra.vratEpilog();
        }
            vstup.setDisable(hra.konecHry());
            tlacitkoOdesli.setDisable(hra.konecHry());
            panelVychodu.setDisable(hra.konecHry());
            panelVeci.setDisable(hra.konecHry());
            panelVeciVMistnosti.setDisable(hra.konecHry());

            grafikaOtevri.setDisable(hra.konecHry());
            grafikaBatoh.setDisable(hra.konecHry());
            grafikaNapoveda.setDisable(hra.konecHry());
            grafikaUtec.setDisable(hra.konecHry());
            grafikaZivoty.setDisable(hra.konecHry());
    }

    /**
     * Definuje grafické rozhraní.
     * @param mouseEvent po kliknutí se provede akce.
     */
    @FXML
    private void KlikPanelVychodu(MouseEvent mouseEvent) {
        Prostor cil = panelVychodu.getSelectionModel().getSelectedItem();
        if(cil==null){
            return;
        }
        String prikaz = PrikazJdi.NAZEV + " " + cil.getNazev();
        zpracujPrikaz(prikaz);
    }

    /**
     * Definuje grafické rozhraní.
     * @param mouseEvent po kliknutí se provede akce.
     */
    @FXML
    private void KlikPanelVeci(MouseEvent mouseEvent){
        Vec cil = panelVeci.getSelectionModel().getSelectedItem();
        if(cil==null){
            return;
        }
        String prikaz = PrikazPoloz.NAZEV + " " + cil.getNazev();
        zpracujPrikaz(prikaz);
    }

    /**
     * Definuje grafické rozhraní.
     * @param mouseEvent po kliknutí se provede akce.
     */
    @FXML
    private void KlikPanelVeciVMistnosti(MouseEvent mouseEvent){
        Vec cil = panelVeciVMistnosti.getSelectionModel().getSelectedItem();
        if(cil==null){
            return;
        }
        String prikaz = PrikazSeber.NAZEV + " " + cil.getNazev();
        zpracujPrikaz(prikaz);
    }

    /**
     * Definuje grafické rozhraní.
     * @param actionEvent po kliknutí se provede akce.
     */
    @FXML
    private void nápovědaKlik(ActionEvent actionEvent) {
        Stage napovedaStage = new Stage();
        WebView wv = new WebView();
        Scene napovedaScena = new Scene(wv);
        napovedaStage.setScene(napovedaScena);
        napovedaStage.show();
        wv.getEngine().load(getClass().getResource("napoveda.html").toExternalForm());
    }


    /**
     * Definuje grafické rozhraní.
     * @param mouseEvent po kliknutí se provede akce.
     */
    public void KlikGrafikaBatoh(MouseEvent mouseEvent) {
        String prikaz = PrikazVypisBatoh.NAZEV;
        zpracujPrikaz(prikaz);
    }


    /**
     * Definuje grafické rozhraní.
     * @param mouseEvent po kliknutí se provede akce.
     */
    public void KlikgrafikaUtec(MouseEvent mouseEvent) {
        String prikaz = PrikazUtec.NAZEV;
        zpracujPrikaz(prikaz);
    }


    /**
     * Definuje grafické rozhraní.
     * @param mouseEvent po kliknutí se provede akce.
     */
    public void KlikGrafikaNapoveda(MouseEvent mouseEvent) {
        String prikaz = PrikazNapoveda.NAZEV;
        zpracujPrikaz(prikaz);
    }


    /**
     * Definuje grafické rozhraní.
     * @param mouseEvent po kliknutí se provede akce.
     */
    public void KlikGrafikaZivoty(MouseEvent mouseEvent) {
        String prikaz = PrikazZivoty.NAZEV;
        zpracujPrikaz(prikaz);
    }


    /**
     * Definuje grafické rozhraní.
     * @param mouseEvent po kliknutí se provede akce.
     */
    public void KlikGrafikaOtevri(MouseEvent mouseEvent) {
        String prikaz = PrikazOtevri.NAZEV;
        zpracujPrikaz(prikaz);
    }
}
