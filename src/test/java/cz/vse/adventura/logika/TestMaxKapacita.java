package cz.vse.adventura.logika;

import cz.vse.adventura.logika.Batoh;
import cz.vse.adventura.logika.Vec;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testovací třída TestMaxKapacita ověřuje maximální kapacitu batohu
 *
 * @author Vojtěch Poklop
 * @vesion LS 2023
 */
public class TestMaxKapacita {

    /**
     * Metoda přidává věci do Batohu.
     * Maximální kapacita batohu je nastavena na 3 -> test papírek A pozitivní, test papírek D negativní.
     */
    @Test
    public void pridejDoBatohu(){
        Batoh batoh = new Batoh();
        Vec papirekA = new Vec("papírekA",true);
        Vec papirekB = new Vec("papírekB",true);
        Vec papirekC = new Vec("papírekC",true);

        batoh.pridejVec(papirekA);
        batoh.pridejVec(papirekB);
        batoh.pridejVec(papirekC);

        assertTrue(batoh.getVec("papírekA")!=null);
        //papírek se přidá do batohu
    }
}
