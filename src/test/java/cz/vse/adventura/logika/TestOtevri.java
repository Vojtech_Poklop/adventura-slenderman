package cz.vse.adventura.logika;

import cz.vse.adventura.logika.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testovací třída zkoumá, zdali je možné otevřít dveře.
 *
 * @author Vojtěch Poklop
 * @version LS 2023
 */
public class TestOtevri {
    private Hra hra1;
    private HerniPlan plan;

    @BeforeEach
    public void setUp() {
        hra1 = new Hra();
    }


    @AfterEach
    public void tearDown() {
    }


    @BeforeEach
    public void setUp2() {

        hra1= new Hra();
        Batoh batoh = new Batoh();
        Vec zluteDvere = new Vec("žlutéDveře", false);
        Vec zlutyKlic = new Vec("žlutýKlíč", true);
        batoh.pridejVec(zluteDvere);
        batoh.pridejVec(zlutyKlic);


    }
    @Test
    public void test(){
        assertNotEquals("Žluté dveře se nyní otevřou.", hra1.zpracujPrikaz("otevři"));
    }
}
