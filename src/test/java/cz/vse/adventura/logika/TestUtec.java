package cz.vse.adventura.logika;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestUtec {

    private Hra hra;

    @BeforeEach
    public void setUp() {
        hra = new Hra();
    }

    @Test
    public void testUteciBezPapirku() {
        Prostor prostor = new Prostor("testovaciProstor", "Testovací prostor");
        hra.getHerniPlan().setAktualniProstor(prostor);
        prostor.addVec(new Vec("vrata", false));
        String vysledek = hra.zpracujPrikaz("uteč");

        assertTrue(vysledek.contains("Nemůžeš utéct, u východu chybí následující papírky"));
    }
}
