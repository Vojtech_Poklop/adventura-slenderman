package cz.vse.adventura.logika;

import cz.vse.adventura.logika.Batoh;
import cz.vse.adventura.logika.HerniPlan;
import cz.vse.adventura.logika.Hra;
import cz.vse.adventura.logika.Vec;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.function.BooleanSupplier;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Testovací třída TestSeber ověřuje, zdali jdou do batohu přidávat věci přenositelné a nejdou věci nepřenositelné
 *
 *
 * @vesion LS 2023
 */

public class TestSeber {

    private Hra hra1;
    private HerniPlan plan;
    /**
     * Metoda se snaží přidat věci do Batohu. Je úspěšná v závislosti na přenositelnosti a nepřenositelnosti objektu.
     */



    @Test
    public void pridejDoBatohu(){
        hra1 = new Hra();
        Batoh batoh = new Batoh();
        Vec papirekA = new Vec("papírekA",true);
        Vec zluteDvere = new Vec("žlutéDveře", false);

        batoh.pridejVec(papirekA);
        batoh.pridejVec(zluteDvere);


        assertNotNull(batoh.getVec("papírekA"));
        //úspěšný test, papírek se nachází v batohu

        assertFalse(hra1.getHerniPlan().getBatoh().veciVBatohu.contains("žlutéDveře"));
        //dveře, které nejdou přidat, se do batohu nepřidají

    }
    }

